package com.tcs.demo.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"name"})})
public class TodoTask {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(unique = true)
    private String name;

    private boolean completed;
    private int priority;
    private Date dateTime;
    private String description;

    public String getPriority() {
        return priority > 5 ? "High" : "Low";
    }

}

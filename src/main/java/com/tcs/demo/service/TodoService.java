package com.tcs.demo.service;

import com.tcs.demo.entity.TodoTask;
import com.tcs.demo.jpa.TodoTaskRepository;
import com.tcs.demo.view.IncompleteTaskView;
import com.tcs.demo.view.TodoTaskView;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class TodoService {

    @Autowired  // dependency injection  //aspect oriented programming // inversion of control
    private TodoTaskRepository todoTaskRepository;

    public List<TodoTask> listTasks() {
        List<TodoTask> tasks =  todoTaskRepository.findAll();
        // SELECT * FROM todo_task;
        log.info("Returning list of tasks, tasks size: {}", tasks.size());
        return tasks;
    }

    // 1. Use views
    // 2. save is used for insert and update
    public void createTask(IncompleteTaskView taskView) {
        log.info("Creating task");
        TodoTask newTask = new TodoTask();
        newTask.setName(taskView.getName());
        newTask.setPriority(taskView.getPriority());
        todoTaskRepository.save(newTask);
        //INSERT INTO todo_task(id, name, completed....) VALUES((1, Spring Session, TRUE....));
        log.info("Task Created");
    }

    public void updateTask(TodoTaskView taskView) {
        log.info("Updating task");
        // UPDATE todo_task
        // SET completed=TRUE, description="Description Testing"
        // WHERE id={taskView.getId()};
        // SELECT * FROM todo_task WHERE id=?;
        Optional<TodoTask> task = todoTaskRepository.findById(taskView.getId());
        if (task.isPresent()) {
            TodoTask taskToBeUpdated = task.get();
            taskToBeUpdated.setCompleted(taskView.isCompleted());
            todoTaskRepository.save(taskToBeUpdated);
        } else {
            throw new RuntimeException("Doesn't exist in DB");
        }
        log.info("Updated the task");
    }

    public void deleteTask(Long taskId) {
        log.info("Deleting task");
        Optional<TodoTask> task = todoTaskRepository.findById(taskId);
        if (task.isPresent()) {
            // DELETE FROM todo_task WHERE id=taskId
            todoTaskRepository.delete(task.get());
        } else {
            throw new RuntimeException("Doesn't exist in DB");
        }
        log.info("Deleted the task");
    }

}

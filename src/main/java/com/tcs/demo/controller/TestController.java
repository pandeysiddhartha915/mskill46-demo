package com.tcs.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {

    //http://localhost:8080/test/book/search/sh
    @GetMapping("/book/search/{data}")
    public String search(@PathVariable("data") String searchTerm) {
        return searchTerm;
    }

}

package com.tcs.demo.controller;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/book")
public class MainController {

    private List<String> names = new ArrayList<>();

    //http://localhost:8080/book/search/harryPotter/100?name=Dhananjay
    @GetMapping("/search/{searchTerm}/{limit}")
    public List<String> search(@PathVariable("searchTerm") String query,
                               @PathVariable("limit") Integer maxSize,
                               @RequestParam String name) {
        System.out.println(query + " " + maxSize + " " + name);
        return null;
    }

    @PostMapping(value = "/add/name")
    public void addName(@RequestParam String name, @RequestParam Integer age) {
        names.add(name);
    }

    @GetMapping("/fetch/names")
    public List<String> fetchNames() {
        return names;
    }

    @RequestMapping(value = "/", method = {RequestMethod.POST, RequestMethod.GET, RequestMethod.DELETE})
    public String hello(String name, String age, Integer experience) {
        return "Hello " + name + " !";
    }

}

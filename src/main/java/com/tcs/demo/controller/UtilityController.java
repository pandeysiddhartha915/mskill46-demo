package com.tcs.demo.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/authors")
public class UtilityController {

    private List<String> names = new ArrayList<>();

    @PostMapping(value = "/add/name")
    public void addName(@RequestParam String name) {
        names.add(name);
    }

}

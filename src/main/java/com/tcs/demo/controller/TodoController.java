package com.tcs.demo.controller;

import com.tcs.demo.entity.TodoTask;
import com.tcs.demo.service.TodoService;
import com.tcs.demo.view.IncompleteTaskView;
import com.tcs.demo.view.TodoTaskView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/todo")
public class TodoController {

    @Autowired
    private TodoService todoService;

    @GetMapping("/read")
    public List<TodoTask> fetchList() {
        return todoService.listTasks();
    }

    @PostMapping("/create")
    public void createTask(@RequestBody IncompleteTaskView taskView) {
        todoService.createTask(taskView);
    }

    @PutMapping("/update")
    public void updateTask(@RequestBody TodoTaskView taskView) {
        todoService.updateTask(taskView);
    }

    @DeleteMapping("/delete/{taskId}")
    public void deleteTask(@PathVariable("taskId") Long taskId) {
        todoService.deleteTask(taskId);
    }

    @DeleteMapping("/deleteAll")
    public void deleteAll() {

    }

    @GetMapping("/formatted")
    public List<IncompleteTaskView> incompleteTaskViews() {
        return null;
    }

    @GetMapping("/formattedUtil")
    public List<IncompleteTaskView> incompleteTaskViewsUtil() {
        return null;
    }

}

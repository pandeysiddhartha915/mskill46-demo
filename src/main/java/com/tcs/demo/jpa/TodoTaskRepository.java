package com.tcs.demo.jpa;

import com.tcs.demo.entity.TodoTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository  // Bean
public interface TodoTaskRepository extends JpaRepository<TodoTask, Long> {

    List<TodoTask> findAllOrderByNameDesc();

    List<TodoTask> findAllByCompletedFalse();

    //SELECT * from todo_task where name=?
    TodoTask findOneByName(String name);

}

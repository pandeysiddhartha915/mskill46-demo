package com.tcs.demo.view;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class TodoTaskView {

    private Long id;
    private boolean completed;

}

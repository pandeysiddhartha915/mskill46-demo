package com.tcs.demo.view;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IncompleteTaskView {

    private Long id;
    private String name;
    private Integer priority;

}
